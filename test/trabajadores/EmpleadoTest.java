/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabajadores;

import java.math.BigDecimal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alejandro
 */
public class EmpleadoTest {
    
    public EmpleadoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testNombre() {
        Empleado empleado= new Empleado();
        empleado.setNombre("Juan");
        assertEquals("Juan",empleado.getNombre());
    }
    @Test
    public void testApellido1() {
        Empleado empleado= new Empleado();
        empleado.setApellido1("Garcia");
        assertEquals("Garcia",empleado.getApellido1());
    }
    
    @Test
    public void testApellido2() {
        Empleado empleado = new Empleado();
        empleado.setApellido2("Martínez");
        assertEquals("Martínez", empleado.getApellido2());
    }

    @Test
    public void testIdentificador() {
        Empleado empleado = new Empleado();
        empleado.setIdentificador(1);
        assertEquals(1, empleado.getIdentificador());
    }

    @Test
    public void testSueldo() {
        Empleado empleado = new Empleado();
        empleado.setSueldo(new BigDecimal("1000"));
        assertEquals(0, empleado.getSueldo().compareTo(new BigDecimal("1000")));
    }

    @Test
    public void testRetencionIRPF() {
        Empleado empleado = new Empleado();
        empleado.setRetencionIRPF(new BigDecimal("0.25"));
        assertEquals(0, empleado.getRetencionIRPF().compareTo(new BigDecimal("0.25")));
    }
    @Test
    public void testCalcularCantidadAPagarle() {
        Empleado empleado = new Empleado();
        empleado.setSueldo(new BigDecimal("0.25"));
        empleado.setRetencionIRPF(new BigDecimal("0.25"));
        assertEquals(0, empleado.getRetencionIRPF().compareTo(new BigDecimal("0.25")));
    }
}
