/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabajadores;

import java.math.BigDecimal;

/**
 *
 * @author Alejandro
 */
public class Empleado extends Trabajador {

    private BigDecimal sueldo, retencionIRPF;

    public String getIRPF() {
        return null;
    }

    public void setSueldo(BigDecimal sueldo) {
        this.sueldo = sueldo;
    }

    public BigDecimal getSueldo() {
        return sueldo;
    }

    public void setRetencionIRPF(BigDecimal RetencionIRPF) {
        this.retencionIRPF = RetencionIRPF;
    }

    public BigDecimal getRetencionIRPF() {
        return retencionIRPF;
    }

    @Override
    public BigDecimal calcularCantidadAPagarle() {
        return sueldo.multiply(BigDecimal.ONE.subtract(this.retencionIRPF));

    }

}
