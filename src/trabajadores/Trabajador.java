/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabajadores;

import java.math.BigDecimal;

/**
 *
 * @author Alejandro
 */
public abstract class Trabajador {
    public String apellido1;
    public String apellido2;
    protected int identificador;
    public String nombre;

    abstract public BigDecimal calcularCantidadAPagarle();

    Object getApellido1() {
        return apellido1;
    }

    /**
     * @return the apellido2
     */
    public String getApellido2() {
        return apellido2;
    }

    public int getIdentificador() {
        return identificador;
    }

    String getNombre() {
        return nombre;
    }

    void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    /**
     * @param apellido2 the apellido2 to set
     */
    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
}
